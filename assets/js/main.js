/*
Theme Name: Grumium
GitHub Repo: https://gitlab.com/t3rr0rz0n3/grumium-theme/
Description: A minimalist theme for Ghost
Author: T3rr0rz0n3
Author URI: https://voidnull.es
*/

// COMMUNITY SECTION 
if (typeof show_community == 'undefined') {
    var show_community = false;
};

if (show_community) {
    $('.section-community').removeClass('is-hidden');
};

// BUTTONS FROM NAVBAR (LOGIN)
if (typeof show_buttons_login == 'undefined') {
    var show_buttons_login = false;
};

if (show_buttons_login) {
    $('.is-grumium-login').removeClass('is-hidden');
    $('.is-grumium-register').removeClass('is-hidden');
};

// SHOW / HIDE DROPDOWNS
var dropdown = document.querySelector('.js-super-menu');

dropdown.addEventListener('click', function(event) {
    event.stopPropagation();
    dropdown.classList.toggle('is-active');
});
document.addEventListener('click', function(event) {
    dropdown.classList.remove('is-active');
});

// POST ARCHIVE (PAGE): Add year and month
// Year & Month Break
var yearArray = new Array();
var monthObj = new Object();
$(".post-archive-item").each(function() {
    var archivesYear = $(this).attr("year");
    var archivesMonth = $(this).attr("month");
    yearArray.push(archivesYear);
    if (archivesYear in monthObj) {
        monthObj[archivesYear].push(archivesMonth);
    } else {
        monthObj[archivesYear] = new Array();
        monthObj[archivesYear].push(archivesMonth);
    }
});
var uniqueYear = $.unique(yearArray);
for (var i = 0; i < uniqueYear.length; i++) {
    var html = "<hr><h2>" + uniqueYear[i] + "</h2>";
    $("[year='" + uniqueYear[i] + "']:first").before(html);
    var uniqueMonth = $.unique(monthObj[uniqueYear[i]]);
    for (var m = 0; m < uniqueMonth.length; m++) {
        var html = "<h4 class=\"is-uppercase is-nunito-light\">" + uniqueMonth[m] + "</h4>";
        $("[year='" + uniqueYear[i] + "'][month='" + uniqueMonth[m] + "']:first").before(html);
    }
}

// SEARCH FORM: Open and Close search form
if (typeof show_search == 'undefined') {
    var show_search = false;
}

$(".modal-open-search").click(function() {
    $("#" + $(this).data("target")).addClass('is-active').slideDown("slow");
});

if (!show_search) {
    $(".modal-search").hide();
} else {
    $(".card-search-close").hide();
    $(".result-search").hide();
}

// click search button event

$("#search-input").click(function(event) {
    $(".search-info-txt").hide();
    $(".search-info-count").show();
    $(".search-info-results").show();
    $("#search-input").val("");
    $('#search-input').focus();
    $("#search-results").show();
    event.stopPropagation();
});

// click close button event
$("#search-close").click(function(event) {
    $("#modal-search").slideUp();
    $("#search-results").hide();
    $("#search-input").val(""); //clear search field text
    $(".search-info-txt").show();
    $(".search-info-count").hide();
    $(".search-info-results").hide();
    $(".search-info-count").text('0');
    event.stopPropagation();
});

// click outside of search form event
$(document).mouseup(function(e) {
    var container = $("#card-search-form");

    // if the target of the click isn't the container nor a descendant of the container
    if (show_search && !container.is(e.target) && container.has(e.target).length === 0) {
        //container.hide();
        $("#search-results").hide();
    }
});

let ghostSearch = new GhostSearch({
    key: search_key, // set var search_key in Site Header
    url: search_url, // set var search_url in Site Header
    version: 'v3',
    input: '#search-input',
    results: '#search-results',
    options: {
        keys: [
            'title',
            'plaintext',
        ],
    },
    template: function(result) {
        let url = [location.protocol, '//', location.host].join('');
        let pubDate = new Date(result.published_at).toLocaleDateString(document.documentElement.lang, {
            year: "numeric",
            month: "long",
            day: "numeric"
        });
        return '<div class="result-item"><a href="' + url + '/' + result.slug + '/"><div class="result-title is-nunito-black">' + result.title + '</div><div class="result-date is-nunito-light is-uppercase">' + pubDate + '</div></a>';
    },
    api: {
        resource: 'posts',
        parameters: {
            limit: 'all',
            fields: ['title', 'plaintext', 'published_at', 'slug'],
            filter: '',
            include: '',
            order: 'published_at DESC',
            formats: '',
        },
    },
    on: {
        afterDisplay: function(results) {
            var $searchCount = $('.search-info-count');
            $searchCount.text(results.length);
            /*if (results.total == 0 && document.getElementById('search-input').value != '') {
                let resultsElement = document.getElementById('search-results');
                resultsElement.innerHTML = "<p><a class='search-no-items'>{{t 'No results'}}</a></p>";
                resultsElement.appendChild(e.firstChild);
            };*/
        }
    }
})

// scroll event
$(window).scroll(function() {
    //$("#search-form").hide();
    //$("#search-results").hide();
    $("#search-btn").show();
});

// SECTION FEATURED: SHOW or HIDDEN SECTION FEATURED
// GO TO TOP BUTTON
var btn = $('#gototop');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    }, '300');
});

// MODAL FEDIVERSE
$(".fediverse").click(function() {
    $("#" + $(this).data("target")).addClass('is-active').show("slow");
});

$('.modal-close').click(function() {
    $('.modal').removeClass('is-active');
});

$('.modal-background').click(function() {
    $('.modal').removeClass('is-active');
});

// SHARE CONTENT WITH GNU SOCIAL AND DIASPORA
function fediverseShare() {

    var fediverse_instance = document.getElementById('fediverse-instance').value;
    var fediverse_message = document.getElementById('fediverse-message').value;
    var fediverse_url = document.getElementById('fediverse-url').placeholder;
    var fediverse_flavor = document.getElementById('fediverse-flavor').className;

    var httpProtocol = fediverse_instance.substring(0, 5);
    if (fediverse_instance == "" || httpProtocol == 'http:') {
        $('#fediverse-instance').addClass('is-danger');
        $(fediverse_instance).focus();
    } else {
        if (httpProtocol == 'https') {
            $('#fediverse-instance').removeClass('is-danger');
            var share_action = get_share_fediverse(fediverse_flavor, fediverse_message, fediverse_url);
            var urlNewWin = fediverse_instance + share_action;
            OpenInNewTab(urlNewWin);
        } else {
            $('#fediverse-instance').removeClass('is-danger');
            var share_action = get_share_fediverse(fediverse_flavor, fediverse_message, fediverse_url);
            var urlNewWin = 'https://' + fediverse_instance + share_action;
            OpenInNewTab(urlNewWin);
        }
    }
}

function get_share_fediverse(fediverse_flavor, fediverse_message, fediverse_url) {
    console.log(fediverse_url);
    if (fediverse_flavor == "gnusocial") {
        return '/?action=newnotice&status_textarea=' + fediverse_message + ' ' + fediverse_url;

    } else if (fediverse_flavor == "mastodon") {
        return '/share?text=' + fediverse_message + '&url=' + fediverse_url;

    } else if (fediverse_flavor == "pleroma") {
        return null;

    } else if (fediverse_flavor == "hubzilla") {
        return '/rpost?body=' + fediverse_message + '&url=' + fediverse_url;

    } else if (fediverse_flavor == "friendica") {
        return null;

    } else if (fediverse_flavor == "diaspora") {
        return '/bookmarklet?url=' + fediverse_url + '&title=' + fediverse_message + '&jump=doclose';

    } else {
        alert("ERROR");
    };
};

$('#fediverse-family-box>div>button').click(function() {
    $('#fediverse-family-box>div>button.tile').removeClass('active');
    $(this).addClass('active');
});

function update_fediverse_family(fediverse_flavor) {
    $('#fediverse-flavor').removeClass();
    $('#fediverse-flavor').addClass(fediverse_flavor);
}

function OpenInNewTab($url) {
    var win = window.open($url, '_blank');
    win.focus();
}


// SWITCH THEME DARK/LIGTH
var toggle = document.getElementById("theme-toggle");

var storedTheme = localStorage.getItem('theme') || (window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light");
if (storedTheme)
    document.documentElement.setAttribute('data-theme', storedTheme)


toggle.onclick = function() {

    var currentTheme = document.documentElement.getAttribute("data-theme");
    var targetTheme = "light";

    if (currentTheme === "light") {
        targetTheme = "dark";
        $('#theme-toggle span.switch i').removeClass('gg-moon').addClass('gg-sun');
    } else {
        $('#theme-toggle span.switch i').removeClass('gg-sun').addClass('gg-moon');

    }

    document.documentElement.setAttribute('data-theme', targetTheme)
    localStorage.setItem('theme', targetTheme);
};


/*function switch_theme_dark_light() {
    var element = document.body;
    element.classList.toggle("dark-mode");
}*/

// NAVBAR BURGER


$(document).ready(function() {

    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");

        document.addEventListener('DOMContentLoaded', function() {

            // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any nav burgers
            if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach(function($el) {
                    $el.addEventListener('click', function() {

                        // Get the target from the "data-target" attribute
                        var target = $el.dataset.target;
                        var $target = document.getElementById(target);

                        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                        $el.classList.toggle('is-active');
                        $target.classList.toggle('is-active');

                    });
                });
            }

        });

    });
});