/*
Theme Name: Grumium
GitHub Repo: https://gitlab.com/t3rr0rz0n3/grumium-theme/
Description: A minimalist theme for Ghost
Author: T3rr0rz0n3
Author URI: https://voidnull.es
*/

// PROGRESS SCROLL
window.onscroll = function() {
    progressBar()
};

function progressBar() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("js-progress-bar").style.width = scrolled + "%";
}

// TOC SETTINGS
if (typeof show_toc == 'undefined') {
    var show_toc = false; //default: false
};
// hide toc background by default
if (!show_toc) {
    $("#table-of-content").hide();
};

// Sticky TOC Scroll effect
$(window).scroll(function() {
    var pxTocImg = 500; // for post with featured image
    if ($(window).scrollTop() < pxTocImg) {
        $("#table-of-content").addClass('is-absolute');
        $("#table-of-content").removeClass('is-sticky').removeClass('is-hidden');
    };
    if ($(window).scrollTop() >= pxTocImg) {
        $("#table-of-content").removeClass('is-absolute');
        $("#table-of-content").addClass('is-sticky');
    };
});

if (show_toc) {
    tocbot.init({
        tocSelector: '.js-toc',
        contentSelector: '.js-content',
        headingSelector: 'h2, h3, h4',
        includeHtml: true,
        orderedList: false,

    });
}

if ($("#table-of-content").children().length > 1) {
    $("#table-of-content").removeClass('is-hidden');
}