# CHANGELOG

## [v0.1.7](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.7) (27/08/2020)
- Update Theme version 0.1.7
-

## [v0.1.6](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.6) (27/02/2020)
- Add subscribe section
- Add mega menu

## [v0.1.5](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.5) (04/02/2020)
- [REMOVE] Data and Author on card post in home.
- [ADD] Zoom In Effect on posts in home.
- [CHANGE] Add 6 posts in featured section.
- [UPDATE] Search section
- [ADD] New design from aside section.
- [UPDATE] TOC in Single Post.

## [v0.1.4](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.4) (19/12/2019)
- [ADD] Search in home
- [CHANGE] Design home.
- [ADD] New Design single post.
- [ADD] Subscribe section.
- [ADD] Share content section.
- [ADD] New Footer.
- [REMOVE] Locales.
- [ADD] Infinite Scroll.


## [v0.1.3](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.3) (23/11/2019)
- [CHANGE] Georgia font for Share Tech Mono and Montserrat Font for NittiBold and Nitti Font
- [REMOVE] Badge CSS.
- [REMOVE] Carousel.
- [ADD] New design to showcase.
- [ADD] New design to list-card.


## [v0.1.2](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.2) (31/10/2019)
- [CHANGE] post.hbs for custom-full-width
- [ADD] custom-not-full-width
- [ADD] Template for head tag
- [REMOVE] Remove file post_footer.
- [REMOVE] Remove post_footer include.
- [REMOVE] Remove post_toc.hbs
- [ADD] Add Spanish locale.


## [v0.1.1](https://gitlab.com/t3rr0rz0n3/grumium-theme/-/tags/v0.1.1) (31/10/2019)
- [UPDATE] Change licence (MIT -> GPL)
- [REMOVE] Katex JS.
- [REMOVE] "Fizzy Credit" code.
- [REMOVE] Katex code in theme.
- [ADD] New meta file in partials folder.
- [UPDATE] Head tag.
- [REMOVE] IconFont
- [ADD] Fork Awesome Font
- [UPDATE] Prims JS and CSS
- [UPDATE] Clean JS in Default and main.js

## [v0.1](https://gitlab.com/t3rr0rz0n3/grumium-theme)
- First release
